import { createFileConfig } from '@archer-oss/dev-scripts/src/rollup.config';

export default createFileConfig({
  input: 'src/dependency-checker.context.tsx',
  external: ['react', '@archer-oss/dependency-checker-core', /@babel\/runtime/],
});
