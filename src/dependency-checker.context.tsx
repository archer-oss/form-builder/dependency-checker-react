import * as React from 'react';
import {
  merge,
  uniq,
  BaseData,
  BaseMetaData,
  DataCollection,
  MetaDataCollection,
  DependenciesCollection,
  EffectsCollection,
} from '@archer-oss/dependency-checker-core';
import { useEffectReducer, EffectReducer } from 'use-effect-reducer';
// TODO: Replace with internal isEqual util
// @ts-ignore
import isEqual from 'lodash.isequal';
import {
  dependencyCheckerProviderActionTypes,
  DataSubscriber,
  DependencyCheckerProviderEvent,
  DependencyCheckerProviderDispatch,
  DependencyCheckerProviderEffect,
  DependencyCheckerProviderProps,
  DependencyCheckerProviderState,
  DependentKeySubscriber,
  ResetKeys,
  ResetStateRef,
  ResetConfig,
  ResetStateUpdater,
  StateUpdater,
} from './dependency-checker.context.type';
import { makeCancelable, noop, useCancelableDependencyCheckerHandler, useResetStateRef } from './utils';

const dependencyCheckerProviderReducer: EffectReducer<
  DependencyCheckerProviderState,
  DependencyCheckerProviderEvent,
  DependencyCheckerProviderEffect
> = (state, event, exec) => {
  const { type } = event;
  let nextState = { ...state };

  switch (event.type) {
    // Add a subscriber that will be notified when the data of any key changes
    case dependencyCheckerProviderActionTypes.subscribeToStateChanges: {
      nextState = {
        ...state,
        dataSubscribers: [...state.dataSubscribers, event.subscriber],
      };

      exec({ type: 'ON_NOTIFY_DATA_SUBSCRIBERS' });

      break;
    }
    case dependencyCheckerProviderActionTypes.unsubscribeFromStateChanges: {
      nextState = {
        ...state,
        dataSubscribers: state.dataSubscribers.filter((dataSubscriber) => dataSubscriber !== event.subscriber),
      };

      break;
    }
    // Add a subscriber that will be notified when the data of a specific key changes (i.e. this is what keeps things fast)
    case dependencyCheckerProviderActionTypes.subscribeToDependentKeyChanges: {
      const { key, subscriber } = event;
      const updatedSubscribers = [...(state.dependentKeySubscribers[key] || []), subscriber];
      nextState = {
        ...state,
        dependentKeySubscribers: {
          ...state.dependentKeySubscribers,
          [key]: updatedSubscribers,
        },
      };

      exec({ type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS', modifiedKeys: [key] });

      break;
    }
    case dependencyCheckerProviderActionTypes.unsubscribeFromDependentKeyChanges: {
      const { key, subscriber } = event;
      const updatedSubscribers = state.dependentKeySubscribers[key]?.filter((dependentKeySubscriber) => dependentKeySubscriber !== subscriber);
      nextState = {
        ...state,
        dependentKeySubscribers: {
          ...state.dependentKeySubscribers,
          [key]: updatedSubscribers,
        },
      };

      if (updatedSubscribers.length === 0) {
        delete nextState.dependentKeySubscribers[key];
      }

      break;
    }
    case dependencyCheckerProviderActionTypes.setDependentKeyData: {
      const { key, dataUpdater = {} } = event;
      nextState = {
        ...state,
        data: {
          ...state.data,
          [key]: {
            ...state.data[key],
            ...(typeof dataUpdater === 'function' ? dataUpdater(state.data[key] as BaseData) : dataUpdater),
          },
        },
      };

      exec({ type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS', modifiedKeys: [key] });
      exec({ type: 'ON_RECALCULATE_DEPENDENCIES', modifiedKeys: [key] });
      exec({ type: 'ON_CHANGE', details: { type: 'key-update', key } });

      break;
    }
    case dependencyCheckerProviderActionTypes.setDependentKeyMetaData: {
      const { key, metaDataUpdater = {} } = event;
      nextState = {
        ...state,
        metaData: {
          ...state.metaData,
          [key]: {
            ...state.metaData[key],
            ...(typeof metaDataUpdater === 'function' ? metaDataUpdater(state.metaData[key] as BaseMetaData) : metaDataUpdater),
          },
        },
      };

      exec({ type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS', modifiedKeys: [key] });
      exec({ type: 'ON_RECALCULATE_DEPENDENCIES', modifiedKeys: [key] });

      break;
    }
    case dependencyCheckerProviderActionTypes.resetData: {
      const { nextData, updater } = event;

      // Set the data with the updater if it exists
      // otherwise overwrite with the incoming data.
      if (updater) {
        nextState = {
          ...state,
          data: typeof updater === 'function' ? updater(nextData, state.data as DataCollection) : updater,
        };
      } else {
        nextState = { ...state, data: nextData };
      }

      // Treat every key as stale since we
      // can't know which keys were updated.
      const modifiedKeys = Object.keys(nextState.data);

      exec({ type: 'ON_NOTIFY_DATA_SUBSCRIBERS' });
      exec({ type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS', modifiedKeys });
      exec({ type: 'ON_RECALCULATE_DEPENDENCIES', modifiedKeys });

      break;
    }
    case dependencyCheckerProviderActionTypes.resetMetaData: {
      const { nextMetaData, updater } = event;

      // Set the metaData with the updater if it exists
      // otherwise overwrite with the incoming metaData.
      if (updater) {
        nextState = {
          ...state,
          metaData: typeof updater === 'function' ? updater(nextMetaData, state.metaData as MetaDataCollection) : updater,
        };
      } else {
        nextState = { ...state, metaData: nextMetaData };
      }

      // Treat every key as stale since we
      // can't know which keys were updated.
      const modifiedKeys = Object.keys(nextState.metaData);

      exec({ type: 'ON_NOTIFY_DATA_SUBSCRIBERS' });
      exec({ type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS', modifiedKeys });
      exec({ type: 'ON_RECALCULATE_DEPENDENCIES', modifiedKeys });

      break;
    }
    // Update the effects for keys that the dependencyChecker calculated
    case dependencyCheckerProviderActionTypes.commitPendingDependencyEffects: {
      const { allPendingEffects, allStaleDependentKeys } = event;
      let fieldKeySubscribersToNotify: string[] = [];
      Object.keys(allPendingEffects).forEach((key) => {
        const pendingEffects = allPendingEffects[key];
        if (!isEqual(pendingEffects, nextState.effects[key])) {
          fieldKeySubscribersToNotify = [...fieldKeySubscribersToNotify, key];
          nextState = {
            ...nextState,
            effects: { ...nextState.effects, [key]: pendingEffects },
          };
        }
      });

      allStaleDependentKeys.forEach((key) => {
        if (nextState.effects[key] !== undefined) {
          fieldKeySubscribersToNotify = [...fieldKeySubscribersToNotify, key];
          nextState = {
            ...nextState,
            effects: { ...nextState.effects, [key]: undefined },
          };
        }
      });

      exec({ type: 'ON_NOTIFY_DATA_SUBSCRIBERS' });
      exec({ type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS', modifiedKeys: fieldKeySubscribersToNotify });

      if (fieldKeySubscribersToNotify.length > 0) {
        exec({ type: 'ON_CHANGE', details: { type: 'dependency-calculation', keys: fieldKeySubscribersToNotify } });
      }

      break;
    }
    // Create a snapshot of the current data and call the relevant handler
    case dependencyCheckerProviderActionTypes.generateSnapshot: {
      exec({ type: 'ON_GET_SNAPSHOT' });

      break;
    }
    /* istanbul ignore next */
    default:
      throw new Error(`${type} is an invalid DependencyCheckerProvider reducer event type.`);
  }

  return nextState;
};

const DependencyCheckerDispatchContext = React.createContext<DependencyCheckerProviderDispatch | undefined>(undefined);

// Use a base object for default values to guarantee the same object
// reference is used for the useEffect below to avoid an infinite loop
const baseObject = {};
const initialState = { data: {}, metaData: {}, effects: {}, dataSubscribers: [], dependentKeySubscribers: {} };

function DependencyCheckerProvider({
  children,
  data = baseObject,
  metaData = baseObject,
  dependencies = baseObject,
  onChange = noop,
  onGetSnapshot = noop,
  dependencyCheckerRate,
  resetConfig = baseObject,
}: React.PropsWithChildren<DependencyCheckerProviderProps>) {
  const dependencyCheckerHandler = useCancelableDependencyCheckerHandler(dependencyCheckerRate);

  const [, dispatch] = useEffectReducer(dependencyCheckerProviderReducer, initialState, {
    ON_NOTIFY_DATA_SUBSCRIBERS: ({ dataSubscribers, data, effects, metaData }) => {
      dataSubscribers.forEach((subscriber) => {
        subscriber(merge({}, data, effects), metaData as MetaDataCollection);
      });
    },
    ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS: ({ data, effects, metaData, dependentKeySubscribers }, { modifiedKeys }) => {
      modifiedKeys.forEach((key) => {
        const subscribers = dependentKeySubscribers[key] ?? [];
        subscribers.forEach((subscriber) => {
          // TODO: Update the merge util to account for undefined
          subscriber(merge({}, data[key] ?? {}, effects[key] ?? {}), metaData[key] ?? {});
        });
      });
    },
    ON_RECALCULATE_DEPENDENCIES: ({ data, metaData, effects }, { modifiedKeys }) => {
      if (dependencyCheckerHandler) {
        dependencyCheckerHandler.handler({
          dependencies: dependencies as DependenciesCollection,
          modifiedKeys,
          currentData: data as DataCollection,
          currentMetaData: metaData as MetaDataCollection,
          currentEffects: effects as EffectsCollection,
          onDependencyCheckerComplete: (pendingEffects, staleDependentKeys) => {
            commitPendingDependencyEffects(dispatch, pendingEffects, staleDependentKeys);
          },
        });
      }
    },
    ON_CHANGE: ({ data, effects, metaData }, { details }) => {
      onChange(merge({}, data, effects), metaData as MetaDataCollection, details);
    },
    ON_GET_SNAPSHOT: ({ data, effects, metaData }) => {
      onGetSnapshot(merge({}, data, effects), metaData as MetaDataCollection);
    },
  });

  // Set up an imperative handle to allow consumers to
  // control when form builder should reset state.
  const resetStateRef = useResetStateRef(resetConfig, { data: data as DataCollection, metaData: metaData as MetaDataCollection });
  React.useImperativeHandle(
    resetStateRef,
    () => ({
      resetData: (updater) => resetData(dispatch, data as DataCollection, updater),
      resetMetaData: (updater) => resetMetaData(dispatch, metaData as MetaDataCollection, updater),
    }),
    [dispatch, data, metaData],
  );

  return <DependencyCheckerDispatchContext.Provider value={dispatch}>{children}</DependencyCheckerDispatchContext.Provider>;
}

/**
 * Context Hooks
 */
function useDependencyCheckerDispatch() {
  const context = React.useContext(DependencyCheckerDispatchContext);
  if (context === undefined) {
    throw new Error('Make sure useDependencyCheckerDispatch is being used within a DependencyCheckerProvider');
  }
  return context;
}

/**
 * Action Handlers
 */
const subscribeToStateChanges = (dispatch: DependencyCheckerProviderDispatch, subscriber: DataSubscriber) => {
  const cancelableSubscriber = makeCancelable(subscriber);
  dispatch({
    type: dependencyCheckerProviderActionTypes.subscribeToStateChanges,
    subscriber: cancelableSubscriber,
  });
  return () => {
    cancelableSubscriber.cancel();
    dispatch({
      type: dependencyCheckerProviderActionTypes.unsubscribeFromStateChanges,
      subscriber: cancelableSubscriber,
    });
  };
};

const subscribeToDependentKeyChanges = (dispatch: DependencyCheckerProviderDispatch, key: string, subscriber: DependentKeySubscriber) => {
  const cancelableSubscriber = makeCancelable(subscriber);
  dispatch({
    type: dependencyCheckerProviderActionTypes.subscribeToDependentKeyChanges,
    key,
    subscriber: cancelableSubscriber,
  });
  return () => {
    cancelableSubscriber.cancel();
    dispatch({
      type: dependencyCheckerProviderActionTypes.unsubscribeFromDependentKeyChanges,
      key,
      subscriber: cancelableSubscriber,
    });
  };
};

const setDependentKeyData = (dispatch: DependencyCheckerProviderDispatch, key: string, dataUpdater: StateUpdater<BaseData>) =>
  dispatch({
    type: dependencyCheckerProviderActionTypes.setDependentKeyData,
    key,
    dataUpdater,
  });

const setDependentKeyMetaData = (dispatch: DependencyCheckerProviderDispatch, key: string, metaDataUpdater: StateUpdater<BaseMetaData>) =>
  dispatch({
    type: dependencyCheckerProviderActionTypes.setDependentKeyMetaData,
    key,
    metaDataUpdater,
  });

const resetData = (dispatch: DependencyCheckerProviderDispatch, nextData: DataCollection, updater?: ResetStateUpdater<DataCollection>) =>
  dispatch({ type: dependencyCheckerProviderActionTypes.resetData, nextData, updater });

const resetMetaData = (
  dispatch: DependencyCheckerProviderDispatch,
  nextMetaData: MetaDataCollection,
  updater?: ResetStateUpdater<MetaDataCollection>,
) => dispatch({ type: dependencyCheckerProviderActionTypes.resetMetaData, nextMetaData, updater });

const commitPendingDependencyEffects = (
  dispatch: DependencyCheckerProviderDispatch,
  allPendingEffects: Record<string, Record<string, any>>,
  allStaleDependentKeys: string[],
) =>
  dispatch({
    type: dependencyCheckerProviderActionTypes.commitPendingDependencyEffects,
    allPendingEffects,
    allStaleDependentKeys,
  });

const generateSnapshot = (dispatch: DependencyCheckerProviderDispatch) => dispatch({ type: dependencyCheckerProviderActionTypes.generateSnapshot });

/* Re-export types */
export type {
  DependencyCheckerProviderDispatch,
  DependencyCheckerProviderProps,
  ResetKeys,
  ResetStateRef,
  ResetConfig,
  ResetStateUpdater,
  StateUpdater,
};

/* Re-export utils */
export { changedArray } from './utils';

/* Re-export core types */
export type {
  BaseData,
  BaseMetaData,
  BaseEffects,
  BaseDependencies,
  DataCollection,
  MetaDataCollection,
  EffectsCollection,
  DependenciesCollection,
  CreateDependenciesTypes,
} from '@archer-oss/dependency-checker-core';

/**
 * Exports
 */
export {
  DependencyCheckerProvider,
  useDependencyCheckerDispatch,
  subscribeToStateChanges,
  subscribeToDependentKeyChanges,
  setDependentKeyData,
  setDependentKeyMetaData,
  generateSnapshot,
  merge,
  uniq,
};
