import {
  DataCollection,
  MetaDataCollection,
  EffectsCollection,
  BaseData,
  BaseMetaData,
  BaseEffects,
  BaseDependencies,
} from '@archer-oss/dependency-checker-core';

export const dependencyCheckerProviderActionTypes = {
  subscribeToStateChanges: 'SUBSCRIBE_TO_STATE_CHANGES',
  unsubscribeFromStateChanges: 'UNSUBSCRIBE_FROM_STATE_CHANGES',
  subscribeToDependentKeyChanges: 'SUBSCRIBE_TO_DEPENDENT_KEY_CHANGES',
  unsubscribeFromDependentKeyChanges: 'UNSUBSCRIBE_FROM_DEPENDENT_KEY_CHANGES',
  setDependentKeyData: 'SET_DEPENDENT_KEY_DATA',
  setDependentKeyMetaData: 'SET_DEPENDENT_KEY_META_DATA',
  resetData: 'RESET_DATA',
  resetMetaData: 'RESET_META_DATA',
  commitPendingDependencyEffects: 'COMMIT_PENDING_DEPENDENCY_EFFECTS',
  generateSnapshot: 'GENERATE_SNAPSHOT',
} as const;

export type ResetKeys = Partial<Record<'data' | 'metaData', any[]>>;

export type ResetStateRef = React.MutableRefObject<
  | {
      resetData: (updater?: ResetStateUpdater<DataCollection>) => void;
      resetMetaData: (updater?: ResetStateUpdater<MetaDataCollection>) => void;
    }
  | undefined
>;

export type ResetConfig = { resetKeys?: ResetKeys; resetStateRef?: ResetStateRef };

export type DependencyCheckerProviderProps = {
  onChange?: (
    data: DataCollection,
    metaData: MetaDataCollection,
    details: { type: 'key-update'; key: string } | { type: 'dependency-calculation'; keys: string[] },
  ) => void;
  onGetSnapshot?: (data: DataCollection, metaData: MetaDataCollection) => void;
  data: Record<string, BaseData | undefined>;
  metaData: Record<string, BaseMetaData | undefined>;
  dependencies?: Record<string, BaseDependencies | undefined>;
  dependencyCheckerRate?: number;
  resetConfig?: ResetConfig;
};

export type DataSubscriber = (data: DataCollection, metaData: MetaDataCollection) => void;

export type DependentKeySubscriber = (data: BaseData, metaData: BaseMetaData) => void;

export type DependencyCheckerProviderState = {
  data: Record<string, BaseData | undefined>;
  metaData: Record<string, BaseMetaData | undefined>;
  effects: Record<string, BaseEffects | undefined>;
  dataSubscribers: (DataSubscriber & Cancelable)[];
  dependentKeySubscribers: Record<string, (DependentKeySubscriber & Cancelable)[]>;
};

export type DependencyCheckerProviderEffect =
  | { type: 'ON_NOTIFY_DATA_SUBSCRIBERS' }
  | {
      type: 'ON_NOTIFY_DEPENDENT_KEY_SUBSCRIBERS';
      modifiedKeys: string[];
    }
  | {
      type: 'ON_RECALCULATE_DEPENDENCIES';
      modifiedKeys: string[];
    }
  | {
      type: 'ON_CHANGE';
      details: { type: 'key-update'; key: string } | { type: 'dependency-calculation'; keys: string[] };
    }
  | { type: 'ON_GET_SNAPSHOT' };

export type DependencyCheckerProviderEvent =
  | {
      type: typeof dependencyCheckerProviderActionTypes['subscribeToStateChanges'];
      subscriber: DataSubscriber & Cancelable;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['unsubscribeFromStateChanges'];
      subscriber: DataSubscriber & Cancelable;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['subscribeToDependentKeyChanges'];
      key: string;
      subscriber: DependentKeySubscriber & Cancelable;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['unsubscribeFromDependentKeyChanges'];
      key: string;
      subscriber: DependentKeySubscriber & Cancelable;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['setDependentKeyData'];
      key: string;
      dataUpdater: StateUpdater<BaseData>;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['setDependentKeyMetaData'];
      key: string;
      metaDataUpdater: StateUpdater<BaseMetaData>;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['resetData'];
      nextData: DataCollection;
      updater?: ResetStateUpdater<DataCollection>;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['resetMetaData'];
      nextMetaData: MetaDataCollection;
      updater?: ResetStateUpdater<MetaDataCollection>;
    }
  | {
      type: typeof dependencyCheckerProviderActionTypes['commitPendingDependencyEffects'];
      allPendingEffects: EffectsCollection;
      allStaleDependentKeys: string[];
    }
  | { type: typeof dependencyCheckerProviderActionTypes['generateSnapshot'] };

export type DependencyCheckerProviderDispatch = React.Dispatch<DependencyCheckerProviderEvent>;

export type Cancelable = { cancel: () => void };

export type ResetStateUpdater<State> = State | ((nextState: State, prevState: State) => State);

export type StateUpdater<State> = State | ((prevState: State) => State);
