import { isEqual } from '../utils';

it('should handle the isEqual util correctly', () => {
  expect(isEqual()).toBe(true);
});
