import * as React from 'react';
import { CreateDependenciesTypes } from '@archer-oss/dependency-checker-core';
import { render, screen, renderHook, userEvent } from '../test-utils';
import {
  DependencyCheckerProvider,
  DependencyCheckerProviderProps,
  ResetKeys,
  ResetStateRef,
  useDependencyCheckerDispatch,
  subscribeToStateChanges,
  subscribeToDependentKeyChanges,
  setDependentKeyData,
  setDependentKeyMetaData,
  generateSnapshot,
} from '../dependency-checker.context';

describe('DependencyCheckerProvider', () => {
  it('updates values when actions are dispatched or when the data or metaData passed to the provider changes', () => {
    const onChange = jest.fn();
    const { rerender } = render(<DependencyCheckerWrapper onChange={onChange} dependencyCheckerRate={0} />);

    const keyOneInput = screen.getByLabelText(/key one input/i);
    const keyTwoInput = screen.getByLabelText(/key two input/i);

    expect(keyOneInput).toHaveValue('');
    expect(keyTwoInput).toHaveValue('key two initial text');

    userEvent.paste(keyOneInput as HTMLInputElement, 'Key One Input Changed');
    expect(onChange).toHaveBeenCalledTimes(2);
    expect(screen.getByText(/key one field touched/i)).toBeInTheDocument();

    const onChangeData = onChange.mock.calls[1][0];
    expect(onChangeData).toEqual({
      keyOne: { touchedText: 'Key One Field Touched', value: 'Key One Input Changed' },
      keyTwo: { value: 'key two initial text' },
    });

    const onChangeMetaData = onChange.mock.calls[1][1];
    expect(onChangeMetaData).toEqual({ keyOne: { touched: true }, keyTwo: { touched: false } });

    onChange.mockClear();
    rerender(<DependencyCheckerWrapper keyOneValue="Key One Rerender Value" keyTwoValue="Key Two Rerender Value" />);
    expect(keyOneInput).toHaveValue('Key One Rerender Value');
    expect(keyTwoInput).toHaveValue('Key Two Rerender Value');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();
    expect(onChange).toHaveBeenCalledTimes(0);
  });

  it('updates the data and metaData based on the reset keys correctly', async () => {
    const { rerender } = render(<DependencyCheckerWrapper resetKeys={{ data: [], metaData: [] }} />);

    const keyOneInput = screen.getByLabelText(/key one input/i);
    expect(keyOneInput).toHaveValue('');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();

    // Rerendering with the same resetKeys should not reset the data or metaData
    rerender(<DependencyCheckerWrapper keyOneValue="Key One Change One" keyOneTouched resetKeys={{ data: [], metaData: [] }} />);
    expect(keyOneInput).toHaveValue('');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();

    // Rerendering with different data resetKeys should only reset the data
    rerender(<DependencyCheckerWrapper keyOneValue="Key One Change One" keyOneTouched resetKeys={{ data: [true], metaData: [] }} />);
    expect(keyOneInput).toHaveValue('Key One Change One');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();

    // Rerendering with different metaData resetKeys should only reset the metaData
    rerender(<DependencyCheckerWrapper keyOneValue="Key One Change Two" keyOneTouched resetKeys={{ data: [true], metaData: [true] }} />);
    expect(await screen.findByText(/key one field touched/i)).toBeInTheDocument();
    expect(keyOneInput).toHaveValue('Key One Change One');
  });

  it('updates the data and metaData based on the reset state ref correctly', async () => {
    function ResetStateRefWrapper(props: DependencyCheckerWrapperProps) {
      const [dataCount, resetData] = React.useState(0);
      const [metaDataCount, resetMetaData] = React.useState(0);
      const resetStateRef: ResetStateRef = React.useRef();

      // Reset data
      React.useEffect(() => {
        resetStateRef.current?.resetData((nextData) => nextData);
      }, [dataCount]);

      // Reset metaData
      React.useEffect(() => {
        resetStateRef.current?.resetMetaData((nextMetaData) => nextMetaData);
      }, [metaDataCount]);

      return (
        <React.Fragment>
          <button onClick={() => resetData((c) => c + 1)}>Reset Data</button>
          <button onClick={() => resetMetaData((c) => c + 1)}>Reset Meta Data</button>
          <button
            onClick={() => {
              resetData((c) => c + 1);
              resetMetaData((c) => c + 1);
            }}
          >
            Reset Both
          </button>
          <button
            onClick={() => {
              resetStateRef.current?.resetData((_, prevData) => ({
                ...prevData,
                keyOne: { ...prevData.keyOne, value: 'A custom value not coming from props' },
              }));

              resetStateRef.current?.resetMetaData((_, prevMetaData) => ({
                ...prevMetaData,
                keyOne: { ...prevMetaData.keyOne, touched: true },
              }));
            }}
          >
            Update With Custom State
          </button>
          <DependencyCheckerWrapper {...props} resetKeys={{ data: [], metaData: [] }} resetStateRef={resetStateRef} />
        </React.Fragment>
      );
    }

    const { rerender } = render(<ResetStateRefWrapper />);

    const keyOneInput = screen.getByLabelText(/key one input/i);
    expect(keyOneInput).toHaveValue('');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();

    // Forcing the data to reset should only affect the data
    rerender(<ResetStateRefWrapper keyOneValue="Key One Change One" keyOneTouched />);
    userEvent.click(screen.getByText(/reset data/i));
    expect(keyOneInput).toHaveValue('Key One Change One');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();

    // Forcing the metaData to reset should only affect the metaData
    rerender(<ResetStateRefWrapper keyOneValue="Key One Change Two" keyOneTouched />);
    userEvent.click(screen.getByText(/reset meta data/i));
    expect(await screen.findByText(/key one field touched/i)).toBeInTheDocument();
    expect(keyOneInput).toHaveValue('Key One Change One');

    // Forcing the data and metaData to reset should affect both
    rerender(<ResetStateRefWrapper keyOneValue="Key One Change Three" />);
    userEvent.click(screen.getByText(/reset both/i));
    expect(keyOneInput).toHaveValue('Key One Change Three');
    expect(screen.queryByText(/key one field touched/i)).not.toBeInTheDocument();

    // Resetting the state with arbitrary information works
    userEvent.click(screen.getByText(/update with custom state/i));
    expect(await screen.findByText(/key one field touched/i)).toBeInTheDocument();
    expect(keyOneInput).toHaveValue('A custom value not coming from props');
  });

  it('applies effects correctly based on the cond functions', async () => {
    const onChange = jest.fn();
    render(<DependencyCheckerWrapper keyOneValue="Make Key One Valid" onChange={onChange} />);

    const keyOneInput = screen.getByLabelText(/key one input/i);
    const keyTwoInput = screen.getByLabelText(/key two input/i);

    expect(await screen.findByText('Key One Secondary Text')).toBeInTheDocument();
    expect(onChange.mock.calls).toMatchInlineSnapshot(`
      Array [
        Array [
          Object {
            "keyOne": Object {
              "isValid": true,
              "secondaryText": "Key One Secondary Text",
              "value": "Make Key One Valid",
            },
            "keyTwo": Object {
              "value": "key two initial text",
            },
          },
          Object {
            "keyOne": Object {
              "touched": false,
            },
            "keyTwo": Object {
              "touched": false,
            },
          },
          Object {
            "keys": Array [
              "keyOne",
            ],
            "type": "dependency-calculation",
          },
        ],
      ]
    `);

    userEvent.paste(keyOneInput as HTMLInputElement, 'Touched');
    expect(await screen.findByText('Key One Field Touched')).toBeInTheDocument();

    userEvent.paste(keyTwoInput as HTMLInputElement, 'Modified');
    expect(await screen.findByText('Key Two Field Touched')).toBeInTheDocument();

    expect(onChange).toHaveBeenCalledTimes(5);
  });

  it('applies debounced effects correctly based on the cond functions', async () => {
    const onChange = jest.fn();
    render(<DependencyCheckerWrapper dependencyCheckerRate={50} onChange={onChange} />);

    const keyOneInput = screen.getByLabelText(/key one input/i);
    const keyTwoInput = screen.getByLabelText(/key two input/i);

    userEvent.paste(keyOneInput, 'Touched');
    userEvent.paste(keyTwoInput, 'Modified');

    expect(await screen.findByText('Key One Field Touched')).toBeInTheDocument();
    expect(screen.getByText('Key Two Field Touched')).toBeInTheDocument();

    expect(onChange).toHaveBeenCalledTimes(3);
  });

  it('generates a valid data snapshot', async () => {
    const onGetSnapshot = jest.fn();
    render(<DependencyCheckerWrapper keyTwoValue="" onGetSnapshot={onGetSnapshot} />);

    const keyOneInput = screen.getByLabelText(/key one input/i);
    const keyTwoInput = screen.getByLabelText(/key two input/i);

    userEvent.paste(keyOneInput as HTMLInputElement, 'Make Key One Valid');
    expect(await screen.findByText('Key One Secondary Text')).toBeInTheDocument();

    userEvent.paste(keyTwoInput as HTMLInputElement, 'Modified');
    expect(await screen.findByText('Key Two Field Touched')).toBeInTheDocument();

    userEvent.click(screen.getByRole('button', { name: /generate snapshot/i }));

    expect(onGetSnapshot.mock.calls).toMatchInlineSnapshot(`
      Array [
        Array [
          Object {
            "keyOne": Object {
              "isValid": true,
              "secondaryText": "Key One Secondary Text",
              "touchedText": "Key One Field Touched",
              "value": "Make Key One Valid",
            },
            "keyTwo": Object {
              "isValid": true,
              "secondaryText": "Key Two Field Touched",
              "value": "Modified",
            },
          },
          Object {
            "keyOne": Object {
              "touched": true,
            },
            "keyTwo": Object {
              "touched": true,
            },
          },
        ],
      ]
    `);
  });

  it('throws an error if the useDependencyChecker hook is called outside the provider', () => {
    const { result } = renderHook(() => useDependencyCheckerDispatch());
    expect(() => result.current).toThrowErrorMatchingInlineSnapshot(
      `"Make sure useDependencyCheckerDispatch is being used within a DependencyCheckerProvider"`,
    );
  });
});

/**
 * Utilities
 */
type Data = { value?: string; secondaryText?: string; isValid?: boolean; touchedText?: string };
type MetaData = { touched?: boolean };
type BaseDependentConfig = {
  ['type-one']: { type: 'type-one'; data: Data; metaData: MetaData };
  ['type-two']: { type: 'type-two'; data: Data; metaData: MetaData };
};
type Dependencies = CreateDependenciesTypes<BaseDependentConfig>;
type DependentConfig = {
  [key in keyof BaseDependentConfig]: BaseDependentConfig[key] & { key: string; dependencies?: Dependencies };
};

type DependencyCheckerWrapperProps = {
  keyOneValue?: string;
  keyOneTouched?: boolean;
  keyTwoValue?: string;
  keyTwoTouched?: boolean;
  resetKeys?: ResetKeys;
  resetStateRef?: ResetStateRef;
  onChange?: DependencyCheckerProviderProps['onChange'];
  onGetSnapshot?: DependencyCheckerProviderProps['onGetSnapshot'];
  dataSubcriberMock?: jest.Mock<any, any>;
  dependentKeySubscriberMock?: jest.Mock<any, any>;
  dependencyCheckerRate?: number;
};

function DependencyCheckerDataImpl({ dataSubcriberMock, keyToDisplay }: { dataSubcriberMock?: jest.Mock<any, any>; keyToDisplay: string }) {
  const dispatch = useDependencyCheckerDispatch();
  const [keyData, setKeyData] = React.useState<{ data: Record<string, Data>; metaData: Record<string, MetaData> }>({
    data: {},
    metaData: {},
  });

  React.useEffect(() => {
    const unsubscribe = subscribeToStateChanges(dispatch, (data, metaData) => {
      // Invoke the data subscriber mock to determine how many times the subscriber ran
      dataSubcriberMock?.();
      setKeyData({ data, metaData });
    });
    return () => {
      unsubscribe();
    };
  }, [dispatch, dataSubcriberMock]);

  return (
    <div>
      {keyData.data[keyToDisplay]?.isValid ? <p>{keyData.data[keyToDisplay]?.secondaryText}</p> : null}
      {keyData.metaData[keyToDisplay]?.touched && <p>{keyData.data[keyToDisplay]?.touchedText}</p>}
    </div>
  );
}

function DependencyCheckerDependentKeyImpl({ dependentKeySubscriberMock }: { dependentKeySubscriberMock?: jest.Mock<any, any> }) {
  const dispatch = useDependencyCheckerDispatch();
  const [keyOneState, setKeyOneState] = React.useState<Data>({ value: '' });
  const [keyTwoState, setKeyTwoState] = React.useState<Data>({ value: '' });

  React.useEffect(() => {
    const keyOneUnsubscribe = subscribeToDependentKeyChanges(dispatch, 'keyOne', (data) => {
      // Invoke the dependent key subscriber mock to determine how many times the subscriber ran
      dependentKeySubscriberMock?.();
      setKeyOneState(data);
    });
    const keyTwoUnsubscribe = subscribeToDependentKeyChanges(dispatch, 'keyTwo', (data) => {
      // Invoke the dependent key subscriber mock to determine how many times the subscriber ran
      dependentKeySubscriberMock?.();
      setKeyTwoState(data);
    });
    return () => {
      keyOneUnsubscribe();
      keyTwoUnsubscribe();
    };
  }, [dispatch, dependentKeySubscriberMock]);

  return (
    <>
      <label htmlFor="keyOne">
        Key One Input
        <input
          id="keyOne"
          type="text"
          value={keyOneState.value}
          onChange={(event) => {
            const { value } = event.target;
            setDependentKeyData(dispatch, 'keyOne', () => ({ value }));
            setDependentKeyMetaData(dispatch, 'keyOne', { touched: true });
          }}
        />
      </label>

      <label htmlFor="keyTwo">
        Key Two Input
        <input
          id="keyTwo"
          type="text"
          value={keyTwoState.value}
          onChange={(event) => {
            setDependentKeyData(dispatch, 'keyTwo', { value: event.target.value });
            setDependentKeyMetaData(dispatch, 'keyTwo', { touched: true });
          }}
        />
      </label>
      <button type="button" onClick={() => generateSnapshot(dispatch)}>
        Generate Snapshot
      </button>
    </>
  );
}

function DependencyCheckerWrapper({
  keyOneValue = '',
  keyOneTouched = false,
  keyTwoValue = 'key two initial text',
  keyTwoTouched = false,
  resetKeys,
  resetStateRef: externalResetStateRef,
  onChange,
  onGetSnapshot,
  dataSubcriberMock,
  dependentKeySubscriberMock,
  dependencyCheckerRate = 1,
}: DependencyCheckerWrapperProps) {
  const keyData: Record<string, DependentConfig[keyof DependentConfig]['data']> = { keyOne: { value: keyOneValue }, keyTwo: { value: keyTwoValue } };
  const keyMetaData: Record<string, DependentConfig[keyof DependentConfig]['metaData']> = {
    keyOne: { touched: keyOneTouched },
    keyTwo: { touched: keyTwoTouched },
  };
  const keyDependencies: Record<string, DependentConfig[keyof DependentConfig]['dependencies']> = {
    keyOne: [
      { key: 'keyOne', type: 'type-one', cond: (data) => data.value === 'Make Key One Valid', effects: { isValid: true } },
      { key: 'keyOne', type: 'type-one', cond: (data) => !!data.isValid, effects: { secondaryText: 'Key One Secondary Text' } },
      { key: 'keyOne', type: 'type-one', cond: (_, metaData) => !!metaData.touched, effects: { touchedText: 'Key One Field Touched' } },
    ],
    keyTwo: [
      {
        key: 'keyTwo',
        type: 'type-two',
        cond: (_, metaData) => !!metaData.touched,
        effects: { isValid: true, secondaryText: 'Key Two Field Touched' },
      },
    ],
  };

  const resetStateRef: ResetStateRef = React.useRef();

  return (
    <DependencyCheckerProvider
      dependencyCheckerRate={dependencyCheckerRate}
      data={keyData}
      metaData={keyMetaData}
      dependencies={keyDependencies}
      resetConfig={{ resetKeys, resetStateRef: externalResetStateRef ?? resetStateRef }}
      onChange={onChange}
      onGetSnapshot={onGetSnapshot}
    >
      <DependencyCheckerDataImpl keyToDisplay="keyOne" />
      <DependencyCheckerDataImpl dataSubcriberMock={dataSubcriberMock} keyToDisplay="keyTwo" />
      <DependencyCheckerDependentKeyImpl dependentKeySubscriberMock={dependentKeySubscriberMock} />
    </DependencyCheckerProvider>
  );
}
