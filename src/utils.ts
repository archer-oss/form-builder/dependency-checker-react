import { useEffect, useLayoutEffect, useRef } from 'react';
import { createDependencyCheckerHandler, DataCollection, MetaDataCollection } from '@archer-oss/dependency-checker-core';
import { Cancelable, ResetConfig, ResetStateRef } from 'dependency-checker.context.type';

/**
 * Returns true if the prev or next array is undefined, the lengths of the arrays are different or if
 * at least one element in the prev array is different than the element in the next array at the same index.
 */
export function changedArray(prev: any[] | undefined, next: any[] | undefined) {
  if (next === undefined || prev === undefined) return true;
  if (next.length !== prev.length) return true;
  return prev.some((key, index) => !Object.is(key, next[index]));
}

export function isEqual(...args: any[]) {
  return true;
}

/**
 * Wraps a function and makes it cancelable. If the wrapper has been marked as canceled,
 * the canceledReturnValue argument is returned instead of invoking the wrapped function.
 * Ex: Wrap subscribers to prevent any pending reducer effects from calling a recently unsubscribed subscriber.
 * @param fn The function that should be made cancelable
 * @param canceledReturnValue An optional value to return if the function has been marked as canceled
 */
export function makeCancelable<T extends (...args: any) => any>(fn: T, canceledReturnValue?: any) {
  let hasCanceled = false;
  const cancelableFn = (...args: any) => {
    if (!hasCanceled) {
      return fn(...args);
    }
    return canceledReturnValue;
  };
  cancelableFn.cancel = () => {
    hasCanceled = true;
  };
  return cancelableFn as T & Cancelable;
}

export function noop() {}

/**
 * Returns true if the passed array has changed between renders.
 */
export function useResetStateRef(
  { resetKeys, resetStateRef: externalResetStateRef }: ResetConfig,
  { data, metaData }: { data: DataCollection; metaData: MetaDataCollection },
) {
  const internalResetStateRef: ResetStateRef = useRef({ resetData: noop, resetMetaData: noop });
  const resetStateRef = externalResetStateRef ?? internalResetStateRef;

  // Reset all the data or metaData if it changed externally
  // and if one of the resetKeys has changed between renders
  const resetData = useResetKeys(resetKeys?.data);
  const resetMetaData = useResetKeys(resetKeys?.metaData);
  useEffect(() => {
    if (resetData) resetStateRef.current?.resetData(data);
    if (resetMetaData) resetStateRef.current?.resetMetaData(metaData);
  }, [data, metaData, resetData, resetMetaData]);

  return resetStateRef;
}

function useResetKeys(resetKeys: any[] | undefined) {
  const previousResetKeys = usePrevious(resetKeys);
  return changedArray(previousResetKeys, resetKeys);
}

function usePrevious<T>(value: T) {
  const previousRef = useRef<T>();
  useEffect(() => {
    previousRef.current = value;
  });
  return previousRef.current;
}

/**
 * Creates a cancelable dependency checker handler so that
 * pending effects aren't commited after the provider unmounts
 */
export function useCancelableDependencyCheckerHandler(dependencyCheckerRate?: number) {
  const dependencyCheckerHandler = useRef<ReturnType<typeof createDependencyCheckerHandler> | undefined>();
  useLayoutEffect(() => {
    dependencyCheckerHandler.current = createDependencyCheckerHandler(dependencyCheckerRate);
    const { cancel } = dependencyCheckerHandler.current;
    return cancel;
  }, [dependencyCheckerRate]);
  return dependencyCheckerHandler.current;
}
